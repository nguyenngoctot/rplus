jQuery(document).ready(function($){
    var swiper = new Swiper('.slide_home .swiper-container ', {
        spaceBetween: 30,
        centeredSlides: true,
        // loop:true,
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
    });
    var swiper = new Swiper('.slide_home .text2 .swiper-container , .slide_propety .swiper-container ', {
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
    
});