<?php
//Set Session
function startSession() {
    if(!session_id()) {
        session_start();
    }
}
add_action('init', 'startSession', 1);

//Add Style and Script
function add_theme_scripts()
{
    $version = '1.0';
//	wp_enqueue_style( 'FontAwesome', get_template_directory_uri() . '/assets/plugins/linearIcon/linearIcon.css', array(), $version, 'all' );
    wp_enqueue_style('FontAwesome', get_template_directory_uri() . '/assets/plugins/fontAwesome/font-awesome.min.css', array(), $version, 'all');
    wp_enqueue_style('MainStyle', get_template_directory_uri() . '/assets/styles/main.css?ver=1.0', array(), $version, 'all');
//library(tot)
    wp_enqueue_style('fullPagejs', get_template_directory_uri() . '/assets/plugins/fullPage/src/fullpage.css', array(), $version, 'all');
    wp_enqueue_script('fullpage', get_template_directory_uri() . '/assets/plugins/fullPage/src/fullpage.js', array(), $version, true);
    wp_enqueue_script('MainScript', get_template_directory_uri() . '/assets/scripts/main.js', array(), $version, true);
}

add_action('wp_enqueue_scripts', 'add_theme_scripts');

//Include Inc Ajax and shortcode


//Theme Options
if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Theme General Settings',
        'menu_title' => 'Theme Settings',
        'menu_slug'  => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect'   => false
    ));

    acf_add_options_sub_page(array(
        'page_title'  => 'Theme Header Settings',
        'menu_title'  => 'Header',
        'parent_slug' => 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title'  => 'Theme Footer Settings',
        'menu_title'  => 'Footer',
        'parent_slug' => 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title'  => 'Theme Common Settings',
        'menu_title'  => 'Common',
        'parent_slug' => 'theme-general-settings',
    ));

}

//Change Favicon Admin
function favicon4admin()
{
    echo '<link rel="Shortcut Icon" type="image/x-icon" href="' . get_bloginfo('wpurl') . '/wp-content/favicon.png" />';
}

add_action('admin_head', 'favicon4admin');


// Register menu
function rplus_setup()
{
    add_theme_support('post-thumbnails');
    
    register_nav_menus(array(
        'mainmenu'   => esc_html__('Main menu', 'rplus'),
       
    ));
}

add_action('after_setup_theme', 'rplus_setup');


//Remove Default jQuery
if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue()
{
    wp_deregister_script('jquery');
    wp_register_script('jquery', get_template_directory_uri() . '/assets/plugins/jQuery/jquery.min.js', false, null);
    wp_enqueue_script('jquery');
}

/*-----Count View------*/
function getPostViews($postID)
{ // hàm này dùng để lấy số người đã xem qua bài viết
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') { // Nếu như lượt xem không có
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');

        return "0"; // giá trị trả về bằng 0
    }

    return $count; // Trả về giá trị lượt xem
}


function setPostViews($postID)
{// hàm này dùng để set và update số lượt người xem bài viết.
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    } else {
        $count++; // cộng đồn view
        update_post_meta($postID, $count_key, $count); // update count
    }
}

/*------Get Excerpt-----*/
function get_excerpt($limit = 130)
{
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt) >= $limit) {
        array_pop($excerpt);
        $excerpt = implode(" ", $excerpt) . '...';
    } else {
        $excerpt = implode(" ", $excerpt);
    }
    $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);

    return $excerpt;
}
