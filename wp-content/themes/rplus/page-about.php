<?php get_header(); ?>
<link rel="stylesheet" href="<?= home_url(); ?>/wp-content/themes/rplus/assets/styles/about.css?ver=1.2">
    <div class="content_page_about">
        <div id = "fullpage"> 
        <?php if( wp_is_mobile()){ ?> 
            <?php    if(have_rows('banner_page_top_about')){
                    while (have_rows('banner_page_top_about')) : the_row();
                        $img_mobile= get_sub_field('image_background_mobile');
                        $title= get_sub_field('text1');
                        $text= get_sub_field('text2');
            ?>
                <div class=" section banner_top" style="background-image: url('<?= $img_mobile ?>');">
                    <div class="container">
                        <div class="content_banner">
                            <h1 class="title_banner">
                                <?= $title ?>
                                <img src="<?php the_field('logo_r','options'); ?>" alt="logor">
                            </h1>
                            <div class="text"><p><?= $text ?></a></div>
                        </div>
                    </div>
                </div>
            <?php
                endwhile; 
                }
            ?>
        <?php }else{ ?> 
            <?php    if(have_rows('banner_page_top_about')){
                    while (have_rows('banner_page_top_about')) : the_row();
                        $img= get_sub_field('image');
                        $title= get_sub_field('text1');
                        $text= get_sub_field('text2');
            ?>
                <div class=" section banner_top" style="background-image: url('<?= $img ?>');">
                    <div class="container">
                        <div class="content_banner">
                            <h1 class="title_banner">
                                <?= $title ?>
                                <img src="<?php the_field('logo_r','options'); ?>" alt="logor">
                            </h1>
                            <div class="text"><p><?= $text ?></a></div>
                        </div>
                    </div>
                </div>
            <?php
                endwhile; 
                }
            ?>
        <?php }  ?>













            

            <?php 
                    if(have_rows('content_page_about')){
                        while (have_rows('content_page_about')) : the_row();
                            $title= get_sub_field('title');
                            $text= get_sub_field('content');
                ?>
                    <div class=" section content_page_about">
                        <div class="container">
                            <div class="content">
                                <h1 class="title"><?= $title ?></h1>
                                <div class="text"><p><?= $text ?></a></div>
                            </div>
                        </div>
                    </div>
                <?php
                    endwhile;
                    }
            ?>
             <?php 
                if(have_rows('section_footer_about')){
                    while (have_rows('section_footer_about')) : the_row();
                        $img= get_sub_field('bkg_img');
                        $text1= get_sub_field('text1_section');
                        $text2= get_sub_field('text2_section'); 
                        $link= get_sub_field('link_tex2');
                        ?>
                        <div class="section section_bottom" style="background: url(<?= $img; ?>)">
                            <div class="text">
                                <div class="container">
                                    <div class="txt text1">
                                        <div class="img_logo"><img src="<?= get_sub_field('logo_r');?>" alt="bkg_banner"></div>    
                                        <p><?= $text1 ?></p>
                                    </div>
                                    <div class="txt text2"><a href="<?= $link ?>"><?= $text2 ?> <i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
                                </div>
                            </div>
                            <div class="footer_section">
                                    <?php get_footer(); ?>
                            </div>
                        </div>
                    <?php
                    endwhile;
                }
            ?>
            
        </div>
    </div>
    </body>
</html>