<?php get_header();
//$args = array(
//    'post_type'   => 'tktb',
//    'post_status' => 'publish',
//    'order'       => 'DESC',
//    'orderby'     => 'DATE',
//    'showposts'   => 8,
//);
//$listItem = new WP_Query($args)
?>
<!--Style HomePage-->
<link rel="stylesheet"
      href="<?= home_url(); ?>/wp-content/themes/voto/assets/styles/page404.css">
<!--End Style HomePage-->
<section id="primary" class="content-area">
  <main id="main" class="site-main">
    <div id="page404" class="error-404 not-found">
      <h1 class="image404">
        <img src="<?php echo home_url(); ?>/wp-content/themes/voto/assets/images/404.jpg"
             alt=" <?php _e('Trang bạn tìm kiếm không tồn tại', 'flexfit'); ?>">
      </h1>
      <div class="page-content">
        <h2 class="page-title d-none">
          <?php _e('Trang bạn tìm kiếm không tồn tại', 'flexfit'); ?>
        </h2>
      </div>
    </div><!-- .error-404 -->

  </main><!-- #main -->
</section><!-- #primary -->

<?php
get_footer(); ?>


