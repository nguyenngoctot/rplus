<?php get_header(); ?>
<link rel="stylesheet" href="<?= home_url(); ?>/wp-content/themes/voto/assets/styles/search.css">
<?php

$args = [
  "post_status"    => "publish",
  "post_type"      => "post",
  "order"          => "DESC",
  "orderby"        => "DATE",
  "posts_per_page" => "4",
  's'              => $s,
  'paged'          => 1,
];


$getposts = new WP_Query($args);
$num = $getposts->found_posts;

?>
<div class="container-master search-blog">
    <ul class="list-title-category">
        <li>  <?= ($num > 0) ? "Có $num " : "Không có" ?>
            kết quả tìm kiếm cho:
            <h1><?= $s ?></h1>
        </li>
    </ul>
  <?php
  if (have_posts()):
    while ($getposts->have_posts()) : $getposts->the_post();
      ?>
        <div class="blog-item">

            <a href="<?php the_permalink(); ?>" title=" <?php the_title(); ?>"
               class="blog-item-pic">
                   <?php  if( has_post_thumbnail() ) : ?>
                     <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>"
                    alt="<?php the_title(); ?>"/>
                  <?php else :?>
                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/img-blank.png"
                  alt="<?php the_title(); ?>"/>
                  <?php endif; ?>
            </a>
            <div class="blog-item-info">
                <h4 class="font-M blog-title m-bt20">
                    <a href="<?php the_permalink(); ?>" title=" <?php the_title(); ?>">
                      <?php the_title(); ?>
                    </a>
                </h4>
                <span class="blog-subtitle m-bt20">
                  <?php echo get_excerpt(20); ?>
                </span>
                <div class="post-detail-header-subtitle m-bt20">
                    <ul>
                        <li><span class="lnr lnr-calendar-full"></span><?php echo get_the_date('d/m/Y'); ?></li>
                        <li><span class="lnr lnr-user"></span> by <span><?php the_author(); ?></span></li>
                        <li>
                            <span class="lnr lnr-eye"></span><?php echo getPostViews(get_the_ID()); ?><?php _e(' lượt xem') ?>
                        </li>
                    </ul>
                </div>
            </div>

        </div>

    <?php
    endwhile;
    wp_reset_postdata();
  endif;
  ff_corenavi_ajax($getposts);
  ?>

</div>
<?php
?>
<input type="hidden" name="url_ajax" value="<?= admin_url('admin-ajax.php'); ?>">
<input type="hidden" name="search_text" value="<?= $s; ?>">

<?php get_footer(); ?>
<script src='<?= home_url(); ?>/wp-content/themes/voto/assets/scripts/pagination.js'
        type='text/javascript'></script>