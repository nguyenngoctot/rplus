<?php setPostViews(get_the_ID()); ?>
<section id="post-detail-banner">
    <?php if (get_field('banner_blog',"option")): ?>
        <img src="<?php the_field('banner_blog',"option"); ?>" alt="">
    <?php else: ?>
        <img src="<?= home_url(); ?>/wp-content/themes/voto/assets/images/banner-blog.jpg" alt="">
    <?php endif; ?>
</section>
<div class="post-detail-wrapper">
    <div class="container-master">
        <section id="voto-breadcrumb">
            <nav aria-label="breadscrumb">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?= home_url(); ?>">
                            <i class="fa fa-home"></i> <span>Trang chủ</span>
                        </a>
                    </li>
                    <li>
                        <span><?php the_title(); ?></span>
                    </li>
                </ul>
            </nav>
        </section>
        <div class="post-detail-header">
            <h1><?php the_title(); ?></h1>
        </div>
    
        <div class="post-detail-header-subtitle">
            <ul>
                <li><span class="lnr lnr-calendar-full"></span><?php echo get_the_date('d/m/Y'); ?></li>
                <li><span class="lnr lnr-user"></span> by <span><?php the_author(); ?></span></li>
                <li><span class="lnr lnr-eye"></span><?php echo getPostViews(get_the_ID()); ?><?php _e(' lượt xem') ?></li>
            </ul>
        </div>

        <!--Main Post-->
        <div class="post-detail-main">

                <div class="post-detail-main-content">
                    <?php the_content(); ?>
                </div>

        </div>
        <!--End Main Post-->
       
    </div>
</div>