<footer id="footer">
    <div class="container">
        <div class="content_footer">
            <div class="footer_left">
                <div class="Copyright">
                    <?php 
                        if(have_rows('content_copyright','options')):
                            while (have_rows('content_copyright','options')) : the_row();
                                $text= get_sub_field('text_compyright');
                    ?>
                        <p class="text_Copyright"><?= $text ?></p>
                    <?php 
                            if(have_rows('link_footer','options')):
                                while (have_rows('link_footer','options')) : the_row();
                                    $title1= get_sub_field('title_link1');
                                    $link1= get_sub_field('link1'); 
                                    $title2= get_sub_field('text2');
                                    $link2= get_sub_field('link2'); 
                    ?>   
                            <a class="Privacy_Policy" href="<?= $link1 ?>"><?= $title1 ?></a>
                            <a class="Terms_of_Use" href="<?= $link2 ?>"><?= $title2 ?></a>
                    <?php

                                endwhile;
                            endif;
                        endwhile;
                    endif;
                    ?>
                </div>
            </div>
            <div class="footer_right">
                <div class="connect">
                <?php 
                            if(have_rows('connect_with_me','options')):
                                while (have_rows('connect_with_me','options')) : the_row();
                                    $icon= get_sub_field('icon');
                                    $link_connect= get_sub_field('link_connect'); 
                    ?>   
                            <a class="link_connect" href="<?= $link_connect ?>">
                                <img src="<?= $icon ?>" alt="icon">
                            </a>
                    <?php
                                endwhile;
                            endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php
    if (get_field('add_code_to_footer_options', 'option')) {
        the_field('add_code_to_footer_options', 'option');
    }
?>
<?php wp_footer(); ?>
<script>
    if($(window).width() > 1023){ 
        new  fullpage ( '#fullpage' ,  {
            autoScrolling : true ,
            scrollHorizosystem : true,
            onLeave: function(index){
                // console.log(index.index);
                $('.content_home #fullpage .section.content_home.section-1 .text .text1 p , .content_home #fullpage .section.content_home.section-2 .text .text1 p , .content_home #fullpage .section.content_home.section-3 .text .text1 p').css('left','100%');
                $('.content_home #fullpage .section.content_home.section-1 .text .text2 p , .content_home #fullpage .section.content_home.section-2 .text .text2 p , .content_home #fullpage .section.content_home.section-3 .text .text2 p').css('right','100%');
            if(index.index === 0){
                var width= $('.content_home #fullpage .section.content_home.section-1 .text .text1 p').width();
                var widthlast= $('.content_home #fullpage .section.content_home.section-1 .text .text2 p').width();
                $('.content_home #fullpage .section.content_home.section-1 .text .text1 p').css('left','calc(100% - '+ width+'px)');
                $('.content_home #fullpage .section.content_home.section-1 .text .text2 p').css('right','calc(100% - '+ widthlast+'px)');
            }
            if(index.index === 1){
                var width= $('.content_home #fullpage .section.content_home.section-2 .text .text1 p').width();
                var widthlast= $('.content_home #fullpage .section.content_home.section-2 .text .text2 p').width();
                $('.content_home #fullpage .section.content_home.section-2 .text .text1 p').css('left','calc(100% - '+ width+'px)');
                $('.content_home #fullpage .section.content_home.section-2 .text .text2 p').css('right','calc(100% - '+ widthlast+'px)');
            }
            if(index.index === 2){
                var width= $('.content_home #fullpage .section.content_home.section-3 .text .text1 p').width();
                var widthlast= $('.content_home #fullpage .section.content_home.section-3 .text .text2 p').width();
                $('.content_home #fullpage .section.content_home.section-3 .text .text1 p').css('left','calc(100% - '+ width+'px)');
                $('.content_home #fullpage .section.content_home.section-3 .text .text2 p').css('right','calc(100% - '+ widthlast+'px)');

                var width= $('.content_home #fullpage .section.content_home.section-1 .text .text1 p').width();
                var widthlast= $('.content_home #fullpage .section.content_home.section-1 .text .text2 p').width();
                $('.content_home #fullpage .section.content_home.section-1 .text .text1 p').css('left','calc(100% - '+ width+'px)');
                $('.content_home #fullpage .section.content_home.section-1 .text .text2 p').css('right','calc(100% - '+ widthlast+'px)');
            }
            if(index.index === 3){
                var width= $('.content_home #fullpage .section.content_home.section-2 .text .text1 p').width();
                var widthlast= $('.content_home #fullpage .section.content_home.section-2 .text .text2 p').width();
                $('.content_home #fullpage .section.content_home.section-2 .text .text1 p').css('left','calc(100% - '+ width+'px)');
                $('.content_home #fullpage .section.content_home.section-2 .text .text2 p').css('right','calc(100% - '+ widthlast+'px)');
            }
            if(index.index === 4){
                var width= $('.content_home #fullpage .section.content_home.section-3 .text .text1 p').width();
                var widthlast= $('.content_home #fullpage .section.content_home.section-3 .text .text2 p').width();
                $('.content_home #fullpage .section.content_home.section-3 .text .text1 p').css('left','calc(100% - '+ width+'px)');
                $('.content_home #fullpage .section.content_home.section-3 .text .text2 p').css('right','calc(100% - '+ widthlast+'px)');
            }
        }
        });
    };
</script>

