<?php 
/*
Template Name: Tpl Common
*/
?>
<?php get_header(); ?>
<link rel="stylesheet" href="<?= home_url(); ?>/wp-content/themes/rplus/assets/styles/propety.css?ver=1.6">
<link rel="stylesheet" href="<?= home_url(); ?>/wp-content/themes/rplus/assets/styles/get-in-touch.css?ver=1.7">
<link rel="stylesheet" href="<?= home_url(); ?>/wp-content/themes/rplus/assets/plugins/swiper/swiper-bundle.min.css">
<script src="<?= home_url(); ?>/wp-content/themes/rplus/assets/plugins/swiper/swiper-bundle.min.js"></script>
    <?php
        $class="";
        if(is_page('rhualien')){
            $class="rhualien";
        }
        if(is_page('r-skun')){
            $class="r-skun";
        }
        if(is_page('rkohrong')){
            $class="rkohrong";
        }
    ?>
    <div class="content_page_propety1 <?php echo $class; ?>">
        <div id = "fullpage">
            <?php if(get_field('hideshow_section_status') == true): ?>
                <div class="section banner_top_propety">
                    <img class="img-banner dt" src="<?php the_field('image_banner'); ?>" alt="">
                    <img class="img-banner mb" src="<?php the_field('image_banner_mobile'); ?>" alt="">
                    <div class="banner_top">
                        <div class="container">
                            <div class="text">
                                <p class="subtitle"><?php the_field('subtitle'); ?></p>
                                <h1><?php the_field('text_banner_propety1'); ?></h1>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?> 
            <?php if(get_field('hideshow_section_status_box2') == true): ?>
                <div class="section"> 
                    <div class="content_propety1">
                        <img src="<?php the_field('image_box2'); ?>" alt="img_box">
                        <div class="content_box">
                            <div class="center_box">
                                <div class="img_title">
                                    <img src="<?php the_field('img_text1_box2'); ?>" alt="logo" class="logo">
                                </div>
                                <div class="content"><?php the_field('text2_box2'); ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php if(get_field('hideshow_section_status_box3') == true): ?>
                            <?php if(have_rows('content_propety1')){
                                while (have_rows('content_propety1')) : the_row();
                                    $img= get_sub_field('image');
                                    $title= get_sub_field('title_box');
                                    $content= get_sub_field('content_box');
                                    ?>
                                     <div class="section slide_content_propety1">
                                        <div class="content_propety1 ">
                                            <img src="<?= $img ?>" alt="img_box">
                                            <div class="content_box">
                                                <div class="center_box">
                                                    <div class="box box_title">
                                                        <h2 class="title"><?= $title ?></h2>
                                                    </div>
                                                    <div class="box box_content">
                                                        <p class="content"><?= $content ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                endwhile;
                            }
                            ?>  
            <?php endif; ?>
            <?php if(get_field('hideshow_section_status_box4') == true): ?>
                <div class="section section4">
                    <div class="slide_propety">
                            <h2 class="title">Facilities</h2>
                            <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    <?php
                                    if(have_rows('image_slide_propety1')){
                                    while (have_rows('image_slide_propety1')) : the_row();
                                    $img= get_sub_field('image');
                                    $name= get_sub_field('name_image');
                                    ?>
                                    <div class="swiper-slide">
                                        <div class="item_slide">
                                            <img src="<?= $img; ?>" alt="img_slide_propety">
                                            <div class="name_image">
                                                <h2><?= $name ;  ?></h2>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    endwhile;
                                    }
                                    ?>
                                </div>
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                            </div>
                    </div>
                </div>
            <?php endif; ?>
                <div class="section section_bottom_propety">
                    <div class="get_in_touch ">
                        <div class="container">
                            <div class="section_left">
                                <?php 
                                        if(have_rows('content_section_left' , 217)){
                                            while (have_rows('content_section_left' , 217)) : the_row();
                                                $img= get_sub_field('logo_r');
                                                $title_l= get_sub_field('title_section');
                                                $title_r= get_sub_field('title_right_section');
                                                $text= get_sub_field('content_section');
                                                
                                ?>
                                    <div class="content_section">
                                        <h1 class="title">
                                            <?= $title_l ?> <img src="<?= $img ?>" alt="logo_r"><?= $title_r ?>
                                        </h1>
                                        <p class="content"><?= $text ?></p>
                                    </div>
                                <?php
                                    endwhile; 
                                    }
                                ?>
                            </div>
                            <div class="section_right">
                                    <div class="contact_form">
                                        <?php echo do_shortcode( '[contact-form-7 id="220" title="Form liên hệ 1"]' ); ?>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer_section">
                        <?php get_footer(); ?>
                    </div>
                </div>
           
        </div>
    </div>
<script src="<?= home_url(); ?>/wp-content/themes/rplus/assets/scripts/swiper.js"></script>
</body>
</html>