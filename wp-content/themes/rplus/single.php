<?php get_header();?>

<link rel="stylesheet" href="<?= home_url(); ?>/wp-content/themes/voto/assets/styles/post-detail.css">
<!--End Style NewsDetail-->


<main id="post-detail" class="site-main">
		<?php
		/* Start the Loop */
		while ( have_posts() ) :the_post();
			
			get_template_part( 'template-parts/content/content', 'blog' );
		endwhile; // End of the loop.
		?>

</main><!-- #main -->

<?php get_footer();?>