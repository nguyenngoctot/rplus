<?php function create_shortcode_post(){
    ?>
     <div class="blog-item">
        <a href="<?php the_permalink(); ?>" title=" <?php the_title(); ?>"
        class="blog-item-pic">
            <?php  if( has_post_thumbnail() ) : ?>
                <?php the_post_thumbnail(); ?>
            <?php else :?>
                <img src="<?php bloginfo('template_directory'); ?>/assets/images/img-blank.png"
                 alt="<?php the_title(); ?>"/>
            <?php endif; ?>
        </a>
        <div class="blog-item-info">
            <h4 class="font-M blog-title m-bt20">
                <a href="<?php the_permalink(); ?>" title=" <?php the_title(); ?>">
                    <?php the_title(); ?>
                </a>
            </h4>
            <span class="blog-subtitle m-bt20">
                <?php echo get_excerpt(20); ?>
            </span>
            <div class="post-detail-header-subtitle m-bt20">
                <ul>
                    <li><span class="lnr lnr-calendar-full"></span><?php echo get_the_date('d/m/Y'); ?></li>
                    <li><span class="lnr lnr-user"></span> by <span><?php the_author(); ?></span></li>
                    <li><span class="lnr lnr-eye"></span><?php echo getPostViews(get_the_ID()); ?><?php _e(' lượt xem') ?></li>
                </ul>
            </div>
        </div>

    </div>
    <?php
}

add_shortcode( "postitem", "create_shortcode_post" );
 
?>