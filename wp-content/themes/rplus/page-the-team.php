<?php get_header(); ?>
    <link rel="stylesheet" href="<?= home_url(); ?>/wp-content/themes/rplus/assets/styles/work.css?ver=1.4">
    <div class="page_member">
        <div class="container">
            <div class="content_page_member">
                <div class="row">
                    <div class="content_left">
                            <?php 
                                if(have_rows('content_page_the_team')){
                                    while (have_rows('content_page_the_team')) : the_row();
                                        $title= get_sub_field('title');
                                        $content= get_sub_field('content');
                            ?>
                                <div class="Content"> 
                                        <h1 class="name"><?= $title; ?></h1>
                                        <p class="meta"><?= $content; ?></p>
                                </div>
                            <?php
                                endwhile;
                                }
                            ?>
                    </div>
                    <div class="content_right">
                        <div class="list_member">
                            <?php 
                                if(have_rows('list_member')){
                                    while (have_rows('list_member')) : the_row();
                                        $img= get_sub_field('avatar_member');
                                        $title= get_sub_field('name_member');
                                        $text= get_sub_field('position_member');
                            ?>
                                <div class="item"> 
                                    <div class="name_position_member">
                                        <h2 class="name"><?= $title; ?></h2>
                                        <p class="position"><?= $text; ?></p>
                                    </div>                 
                                </div>
                            <?php
                                endwhile;
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_section">
            <?php get_footer(); ?>
        </div>
    </div>

</body>
</html>