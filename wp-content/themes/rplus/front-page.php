<?php get_header(); ?>
    <link rel="stylesheet" href="<?= home_url(); ?>/wp-content/themes/rplus/assets/styles/frontpage.css?ver=1.1">
    <link rel="stylesheet" href="<?= home_url(); ?>/wp-content/themes/rplus/assets/plugins/swiper/swiper-bundle.min.css">
    <script src="<?= home_url(); ?>/wp-content/themes/rplus/assets/plugins/swiper/swiper-bundle.min.js"></script>
    <div class="content_home">
        <div id = "fullpage">
                <?php if( wp_is_mobile()): ?> 
                        <div class="section slide_home slide_home_mobile">
                                    <?php 
                                    if(have_rows('slide_home')){
                                        while (have_rows('slide_home')) : the_row();
                                            $img= get_sub_field('image_background_mobile');
                                            $text1= get_sub_field('text_lide1');
                                            $text2= get_sub_field('text_lide2'); ?>
                                            <div class="swiper-slide">
                                                <div class="text">
                                                    <div class="container">
                                                        <div class="txt text1">
                                                            <div class="img_logo"><img src="<?php the_field('logo_r');?>" alt="img_bkg1"></div>
                                                            <p><?= $text1 ?></p>
                                                        </div>
                                                        <div class="txt textt2">
                                                            <div class="swiper-container swiper-container-slide-home">
                                                                <div class="swiper-wrapper">
                                                                    <?php 
                                                                            if(have_rows('description_slide_home')){
                                                                                while (have_rows('description_slide_home')) : the_row();
                                                                                $desc1 = get_sub_field('description_slide');
                                                                        ?>
                                                                            <div class="swiper-slide description_slide_home">
                                                                                <div class="box_description">
                                                                                    <h4><?= $desc1 ?></h4>
                                                                                </div>
                                                                            </div>
                                                                        <?php
                                                                                endwhile;
                                                                            }
                                                                        ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="bgSlider">
                                                    <img src="<?= $img; ?>" alt="img_mobile">
                                                </div>
                                            </div>
                                        <?php
                                        endwhile;
                                    }
                                    ?>
                        </div>
                        <?php  else: ?>
                                    <?php 
                                    if(have_rows('slide_home')){
                                        while (have_rows('slide_home')) : the_row();
                                            $img= get_sub_field('img_slide');
                                            $text1= get_sub_field('text_lide1');
                                            $text2= get_sub_field('text_lide2'); ?>
                                        <div class="section slide_home slide_home_desktop " style="background: url(<?= $img; ?>)">
                                            <div class="swiper-slide">
                                                <div class="text">
                                                    <div class="container">
                                                        <div class="txt text1">
                                                            <div class="img_logo"><img src="<?php the_field('logo_r');?>" alt="img_bkg1"></div>
                                                            <p><?= $text1 ?></p>
                                                        </div>
                                                        <div class="txt textt2">
                                                            <div class="swiper-container swiper-container-slide-home">
                                                                <div class="swiper-wrapper">
                                                                    <?php 
                                                                            if(have_rows('description_slide_home')){
                                                                                while (have_rows('description_slide_home')) : the_row();
                                                                                $desc1 = get_sub_field('description_slide');
                                                                        ?>
                                                                            <div class="swiper-slide description_slide_home">
                                                                                <div class="box_description">
                                                                                    <h4><?= $desc1 ?></h4>
                                                                                </div>
                                                                            </div>
                                                                        <?php
                                                                                endwhile;
                                                                            }
                                                                        ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        endwhile;
                                    }
                                    ?>
                <?php  endif;?>
                <?php
                    $num = 1;
                    if(have_rows('section_box_home')){
                            while (have_rows('section_box_home')) : the_row();
                                $img= get_sub_field('bkg_section');
                                $text1= get_sub_field('text1_section');
                                $text2= get_sub_field('text2_section'); ?>
                                <div class="section content_home section-<?= $num ?>" style="background: url(<?= $img; ?>)">
                                    <div class="text">
                                        <div class="container">
                                            <div class="img_logo"><img src="<?php the_field('logo_r');?>" alt="img_bkg2"></div>
                                            <div class="txt text1"><p><?= $text1 ?></p></div>
                                            <div class="txt text2"><p><?= $text2 ?></p></div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            $num++;endwhile;
                    }
                ?>
                <?php 
                if(have_rows('section_footer_home')){
                    while (have_rows('section_footer_home')) : the_row();
                        $img= get_sub_field('bkg_img');
                        $text1= get_sub_field('text1_section');
                        $text2= get_sub_field('text2_section');
                        $link= get_sub_field('link_tex2');
                        ?>
                        <div class="section section_bottom" style="background: url(<?= $img; ?>)">
                            <div class="text">
                                <div class="container">
                                    <div class="txt text1">
                                        <div class="img_logo"><img src="<?php the_field('logo_r');?>" alt="img_bkg3"></div>
                                        <p><?= $text1 ?></p>
                                    </div>
                                    <div class="txt text2"><a href="<?= $link ?>"><?= $text2 ?> <i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
                                </div>
                            </div>
                            <div class="footer_section">
                                <?php get_footer(); ?>
                            </div>
                        </div>
                    <?php
                    endwhile;
                }
                ?>
                
        </div>
    </div>
<script src="<?= home_url(); ?>/wp-content/themes/rplus/assets/scripts/swiper.js"></script>
</body>
</html>