<!DOCTYPE html>
<html  class="no-js" <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset');?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="<?php the_field('favicon_options','options');?>">
    <title><?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
   
    <?php wp_head(); ?>
    <!-- Code add Form ACF HeaderOption -->
    <?php 
        if( get_field("add_code_to_header_options",'option') ){
            the_field("add_code_to_header_options",'option');
        }
    ?>
    <!-- Code add Form ACF HeaderOption -->
</head>
<body <?php body_class();?> >
<?php wp_body_open(); ?>
<input type="hidden" name="isHomePage" value="<?=  is_front_page() ? '1':'0' ?>">
    <header id="header" class="<?= get_post_field( 'post_name', get_post() ); ?>">
        <div class="container_header">
        <div class="logo_header">
                <?php if(is_page('the-team')){?> 
                    <a href="/" class="logo_desktop_team"><img src="<?php echo get_bloginfo('url') ?>/wp-content/themes/rplus/assets/images/r+ logo.png" alt="logo_header"></a>
                    <a href="/" class="logo_mobile_team"><img src="<?php the_field('logo_header_options','options'); ?>" alt="logo_header"></a>
                <?php }else{ ?>
                <a href="/"><img src="<?php the_field('logo_header_options','options'); ?>" alt="logo_header"></a>
                <?php } ?>
        </div>
           
            <div class="menu_header">
                <div class="icon_menu">
                    <img src="<?php echo get_bloginfo('url') ?>/wp-content/themes/rplus/assets/images/Hamburger.png" alt="icon_click">
                </div>
                <div class="button_link">
                    <?php
                        if(have_rows('button_link','option')):
                        while (have_rows('button_link','option')) : the_row();
                        $title= get_sub_field('title_button');
                        $link= get_sub_field('link_button');
                    ?>
                    <a href="<?= $link ?>"><?= $title ?></a>
                    <?php endwhile; endif; ?>
                </div>

                <div class="tab_menu">
                    <?php if (has_nav_menu('mainmenu')):
                        wp_nav_menu(array('theme_location' => 'mainmenu', 'container' => ''));
                        endif; 
                    ?>
                </div>
            </div>
        </div>
    </header>
    <script>
        jQuery(document).ready(function($){
            $('#header .container_header .menu_header .tab_menu .menu').before('<div class="close_menu"><img src="<?php echo get_bloginfo('url') ?>/wp-content/themes/rplus/assets/images/anhtot.png" alt="icon_close"></div>');
            $(document).on('click','.menu_header .icon_menu , .close_menu',function(){
                $('#header .container_header .menu_header .tab_menu').toggleClass('active');
                $('.icon_menu').toggle();
                if($(window).width() < 580){
                    $('body').toggleClass('hidden');
                }
            });
        });
    </script>