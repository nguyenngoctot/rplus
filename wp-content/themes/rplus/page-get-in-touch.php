<?php get_header(); ?>
    <link rel="stylesheet" href="<?= home_url(); ?>/wp-content/themes/rplus/assets/styles/get-in-touch.css?ver=1.3">
    <div id = "fullpage">
        <div class="section">
            <div class="get_in_touch ">
                <div class="container">
                    <div class="section_left">
                        <?php 
                                if(have_rows('content_section_left')){
                                    while (have_rows('content_section_left')) : the_row();
                                        $img= get_sub_field('logo_r');
                                        $title_l= get_sub_field('title_section');
                                        $title_r= get_sub_field('title_right_section');
                                        $text= get_sub_field('content_section');
                                        
                        ?>
                            <div class="content_section">
                                <h1 class="title">
                                    <?= $title_l ?> <img src="<?= $img ?>" alt="logo_r"><?= $title_r ?>
                                </h1>
                                <p class="content"><?= $text ?></p>
                            </div>
                        <?php
                            endwhile; 
                            }
                        ?>
                    </div>
                    <div class="section_right">
                            <div class="contact_form">
                                <?php echo do_shortcode( '[contact-form-7 id="220" title="Form liên hệ 1"]' ); ?>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_section">
            <?php get_footer(); ?>
        </div> 
    </div>


</body>
</html>