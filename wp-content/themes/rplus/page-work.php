<?php get_header(); ?>
    <link rel="stylesheet" href="<?= home_url(); ?>/wp-content/themes/rplus/assets/styles/work.css?ver=1.5">
    <div class="content_page_work">
        <div id = "fullpage"> 
        <?php if( wp_is_mobile()){ ?> 
            <?php 
                    if(have_rows('banner_top_work')){
                        while (have_rows('banner_top_work')) : the_row();
                            $img_mobile= get_sub_field('background_image_mobile');
                            $title= get_sub_field('title');
                            $text= get_sub_field('content');
                    ?>
                    <div class=" section banner_top banner_top_mobile" style="background-image: url('<?= $img_mobile ?>');">
                        <div class="container">
                            <div class="content_banner"> 
                                <h1 class="title_banner"><?= $title ?></h1>
                                <div class="text"><p><?= $text ?></a></div>
                            </div>
                        </div>
                    </div>
                    <?php
                        endwhile;
                        }
            ?>
        <?php }else{?>
            <?php 
                    if(have_rows('banner_top_work')){
                        while (have_rows('banner_top_work')) : the_row();
                            $img= get_sub_field('background_image');
                            $title= get_sub_field('title');
                            $text= get_sub_field('content');
                    ?>
                    <div class=" section banner_top" style="background-image: url('<?= $img ?>');">
                        <div class="container">
                            <div class="content_banner"> 
                                <h1 class="title_banner"><?= $title ?></h1>
                                <div class="text"><p><?= $text ?></a></div>
                            </div>
                        </div>
                    </div>
                    <?php
                        endwhile;
                        }
            ?>
        <?php  } ?>
            <div class=" section content_page_work">
                <div class="container">
                    <div class="content_item">
                        <?php 
                            if(have_rows('content_page_work')){
                                while (have_rows('content_page_work')) : the_row();
                                    $img= get_sub_field('image_thumbnail');
                                    $title= get_sub_field('title_description');
                                    $desc= get_sub_field('description');
                        ?>
                            <div class="item">
                                <div class="thumbnail"><img src="<?= $img ?>" alt="image_desc"></div>
                                <h2 class="title"><?= $title ?></h2>
                                <div class="desc"><p><?= $desc ?></p></div>
                            </div>
                        <?php
                            endwhile;
                            }
                        ?>
                    </div>
                </div>
            </div>

            <?php 
                if(have_rows('banner_bottom_work')){
                while (have_rows('banner_bottom_work')) : the_row();
                    $img= get_sub_field('image_box');
                    $title= get_sub_field('title');
                    $text= get_sub_field('content_box');
                    $title_link= get_sub_field('title_link');
                    $link= get_sub_field('link');
            ?>
                <div class="section banner_bottom"> 
                    <div class="content_propety1">
                        <img src="<?= $img ?>" alt="img_box">
                        <div class="content_box">
                            <div class="center_box">
                                <h2 class="title"><?= $title ?></h2>
                                <div class="content"><?= $text ?></div>
                                <div class="link"><a href="<?= $link ?>"><?= $title_link; ?></a> <i class="fa fa-angle-right" aria-hidden="true"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="footer_section">
                        <?php get_footer(); ?>
                    </div>
                </div>
            <?php
                endwhile;
                }
            ?>
            
        </div>
        
    </div>
    </body>
</html>
