<?php
/**
 * Cấu hình cơ bản cho WordPress
 *
 * Trong quá trình cài đặt, file "wp-config.php" sẽ được tạo dựa trên nội dung 
 * mẫu của file này. Bạn không bắt buộc phải sử dụng giao diện web để cài đặt, 
 * chỉ cần lưu file này lại với tên "wp-config.php" và điền các thông tin cần thiết.
 *
 * File này chứa các thiết lập sau:
 *
 * * Thiết lập MySQL
 * * Các khóa bí mật
 * * Tiền tố cho các bảng database
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Thiết lập MySQL - Bạn có thể lấy các thông tin này từ host/server ** //
/** Tên database MySQL */
define( 'DB_NAME', 'rplus' );

/** Username của database */
define( 'DB_USER', 'root' );

/** Mật khẩu của database */
define( 'DB_PASSWORD', '' );

/** Hostname của database */
define( 'DB_HOST', 'localhost' );

/** Database charset sử dụng để tạo bảng database. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Kiểu database collate. Đừng thay đổi nếu không hiểu rõ. */
define('DB_COLLATE', '');

/**#@+
 * Khóa xác thực và salt.
 *
 * Thay đổi các giá trị dưới đây thành các khóa không trùng nhau!
 * Bạn có thể tạo ra các khóa này bằng công cụ
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Bạn có thể thay đổi chúng bất cứ lúc nào để vô hiệu hóa tất cả
 * các cookie hiện có. Điều này sẽ buộc tất cả người dùng phải đăng nhập lại.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '`,0Lda=b|%.u)]O%*wm<@CmOC~U.=R~3Ue-|d&A~)2ZYk1}?3p)`jd,,5#MnBR}g' );
define( 'SECURE_AUTH_KEY',  'A,wJu&G[4GFW7v?<]J/oe_lS=Do3GmVvhU3NuJh/O5W a#b%O!(rk]z`)jA^p}}f' );
define( 'LOGGED_IN_KEY',    '[2!bn|r-kc$Dg<<@INUa:[Hs-}+Zc(PbDfo8fkZHorWcQCdu/i2{5bSy=S_[>2_X' );
define( 'NONCE_KEY',        'a_O~:1W?uQlOU*dsKoehRM3p)|-6.%weX`VI>1%jaVZ2?/udQfcuzRo*X=jlw9S7' );
define( 'AUTH_SALT',        '=cQ*K^pl*Nz(]_CyPF=i;/d-4.B*8|)oe68iK^0n,f7q3s%5~@k;TdhM6(DUpF6L' );
define( 'SECURE_AUTH_SALT', '|;^xaS`Vm[Vd^g-7$;oIlr),=1)xu5!Trn>,Y >t?!X0:$]W+)kq4fSlg<-8!;mn' );
define( 'LOGGED_IN_SALT',   '40uG{x[NK;s#i-B+{BMwCj~4-VgVlgR%BQu:>G,Lb!Z*Yy,3F)S-Qpl2xu!L56Na' );
define( 'NONCE_SALT',       'kN/z<IQp((pEVOzP!%Q$I.YAJ!m]r!?Gq!{EDSsr)sYqubgGGYYu1-f`!C^V6}YW' );

/**#@-*/

/**
 * Tiền tố cho bảng database.
 *
 * Đặt tiền tố cho bảng giúp bạn có thể cài nhiều site WordPress vào cùng một database.
 * Chỉ sử dụng số, ký tự và dấu gạch dưới!
 */
$table_prefix = 'wp_';

/**
 * Dành cho developer: Chế độ debug.
 *
 * Thay đổi hằng số này thành true sẽ làm hiện lên các thông báo trong quá trình phát triển.
 * Chúng tôi khuyến cáo các developer sử dụng WP_DEBUG trong quá trình phát triển plugin và theme.
 *
 * Để có thông tin về các hằng số khác có thể sử dụng khi debug, hãy xem tại Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Đó là tất cả thiết lập, ngưng sửa từ phần này trở xuống. Chúc bạn viết blog vui vẻ. */

/** Đường dẫn tuyệt đối đến thư mục cài đặt WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Thiết lập biến và include file. */
require_once(ABSPATH . 'wp-settings.php');
